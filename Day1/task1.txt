List Command Terminal
1.Ls : Untuk menampilkan isi dalam sebuah direktori
2.Mkdir : Membuat folder baru
3.Rm : menghapus file/folder
4.Cd : beralih ke direktori tertentu
5.Clear : membersihkan terminal
6.Nano : membuka text editor
7.echo "Ini adalah isi file baru" > file.txt : menambahkan file baru
8.Tasklist : Menampilkan daftar proses yang sedang berjalan.
9.Cp : menyalin file (cp new.txt new-copy.txt)
10.pwd : Menampilkan direktori kerja saat ini.


List Command Git
1.Git init : Membuat repositori Git baru
2.Git remote add origin <url> : Menambahkan remote repository dengan nama tertentu.
3.Git add <file> : Menambahkan file ke dalam area stage untuk persiapan commit.
4.Git commit -m “comment“ :  Membuat commit dengan pesan yang menjelaskan perubahan yang dilakukan.
5.Git push origin main : push project ke repository
6.Git Branch : Menampilkan daftar branch yang ada dalam repositori.
7.Git Log : Menampilkan riwayat commit dalam repositori.
8.Git Diff : Menampilkan perbedaan antara perubahan yang belum di-commit dan versi sebelumnya.
9.Git Status : Menampilkan status repositori saat ini, termasuk file yang telah diubah atau ditambahkan.
10.Git Clone <url> : Menduplikasi repositori Git yang ada ke dalam folder lokal.
11.Git checkout <name-of-branch> : berpindah branch
12.git merge <branch>: Menggabungkan perubahan dari branch lain ke branch saat ini.
13.git reset <file>: Membatalkan perubahan yang belum di-commit pada file tertentu.
