/*
1. Write a function that does the following for the given values: add, subtract, divide and multiply. 
These are simply referred to as the basic arithmetic operations. The variables have to be defined, 
but in this challenge they will be defined for you. All you have to do is check the variables, do some string to integer conversions, use some if conditions, and apply the arithmetic operation.
The numbers and operation are given as strings and should result in an integer value.

Note:
The numbers and operation are given as strings and should result in an integer value.
If the operation results in Infinity, then return "undefined" (e.g. division by zero).
Division results will be rounded down to their integral parts.

Example: 
operation("1",  "2",  "add" ) ➞ 3
operation("4",  "5",  "subtract") ➞ -1
operation("6",  "3",  "divide") ➞ 2
operation("2",  "7",  "multiply") ➞ 14
operation("6",  "0",  "divide") ➞ ‘undefined’

*/

const operation = (num1, num2, op) => {
    let result;
    let num1Int = parseInt(num1);
    let num2Int = parseInt(num2);
  
    if (op === "add") {
      result = num1Int + num2Int;

    } else if (op === "subtract") {
      result = num1Int - num2Int;

    } else if (op === "multiply") {
      result = num1Int * num2Int;

    } else if (op === "divide") {
      if (num2Int === 0) {
        return 'undefined';
      }
      result = Math.floor(num1Int / num2Int);

    } else {
      return 'Invalid operation';
    }
  
    return result;
  }
console.log(operation("2","1","add"));
console.log(operation("4","5","subtract"));
console.log(operation("6","3","divide"));
console.log(operation("2","7","multiply"));
console.log(operation("6","0","divide"));


/*
2. Create a function that takes an array of strings and returns an array 
with only the strings that have numbers in them. If there are no strings containing numbers, 
return an empty array.
Notes : The strings can contain white spaces or any type of characters.
Bonus: Try solving this without RegEx.
Example:
numInStr(["1a", "a", "2b", "b"]) ➞ ["1a", "2b"]
numInStr(["abc", "abc10"]) ➞ ["abc10"]
numInStr(['this IS','10xYZ', 'xy2K77', 'Z1K2W0', 'xYz']) ➞  ['10xYZ', 'xy2K77', 'Z1K2W0']
numInStr(['-/>', '10bc', 'abc ']) ➞  ['10bc']
*/

const numInStr = (arr) => {
    let result = [];

    for(let i = 0; i < arr.length; i++){
        let string = arr[i];
        let hasNumber = false;

        for(let j = 0; j < string.length; j++){
            if(!isNaN(parseInt(string[j]))){
                hasNumber = true;
                break;
            }
        }
        if(hasNumber){
            result.push(string); 
        }
    }
    return result;
}
console.log(numInStr(["1a", "a", "2b", "b"]));
console.log(numInStr(["abc", "abc10"]));
console.log(numInStr(['this IS','10xYZ', 'xy2K77', 'Z1K2W0', 'xYz']));

/*
3. We can assign a value to each character in a word, based on their position 
in the alphabet (a = 1, b = 2, ... , z = 26). A balanced word is one where the 
sum of values on the left-hand side of the word equals the sum of values on the right-hand side. 
For odd length words, the middle character (balance point) is ignored.

Write a function that returns true if the word is balanced, and false if it's not.
Example
balanced("zips") ➞ true
// "zips" = "zi|ps" = 26+9|16+19 = 35|35 = true 
balanced("brake") ➞ false
// "brake" = "br|ke" = 2+18|11+5 = 20|16 = false
*/

const balanced = (word) => {

  let wordArr = [...word], word1 = 0, word2 = 0;

  const alphabet  = "abcdefghijklmnopqrstuvwxyz";
  while(wordArr.length > 1){
    word1 += alphabet.indexOf(wordArr.shift());
    word2 += alphabet.indexOf(wordArr.pop());
  }
  return word1 === word2; 
}
console.log(balanced("zips"));
console.log(balanced("brake"));
console.log(balanced('disillusioned'));

/*
4. 2048 is a game where you need to slide numbered tiles (natural powers of 2) up, down, 
left or right on a square grid to combine them in a tile with the number 2048.

The sliding procedure is described by the following rules:

Tiles slide as far as possible in the chosen direction until they are stopped by either another 
tile or the edge of the grid.If two tiles of the same number collide while moving, 
they will merge into a tile with the total value of the two tiles that collided.
If more than one variant of merging is possible, move direction shows one that will take effect.
Tile cannot merge with another tile more than one time.
Sliding is done almost the same for each direction and for each row/column of the grid, 
so your task is to implement only the left slide for a single row.

Note :
Input row can be of any size (empty too).
Input row will contain only natural powers of 2 and 0 for empty tiles.
Keep trailing zeros in the output.

Example
leftSlide([2, 2, 2, 0]) ➞ [4, 2, 0, 0] // Merge left-most tiles first.
leftSlide([2, 2, 4, 4, 8, 8]) ➞ [4, 8, 16, 0, 0, 0] // Only merge once.

*/
const leftSlide = (row) => {

    var Sorted = row.filter(tile => tile !== 0);
  
    var result = [];
    var merged = false;
  
    for (var i = 0; i < Sorted.length; i++) {
      if (i < Sorted.length - 1 && Sorted[i] === Sorted[i + 1] && !merged) {
        result.push(Sorted[i] * 2);
        merged = true;
      } else {
        // Move tile to the left
        result.push(Sorted[i]);
        merged = false;
      }
    }
  
    while (result.length < row.length) {
      result.push(0);
    }
  
    return result;
  }
  console.log(leftSlide([2, 2, 2, 0]));