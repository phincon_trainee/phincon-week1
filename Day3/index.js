import fetch from "node-fetch";
/*
1. Create countdown program to display remaining time, input will be number in second
    Example: countdown(5402);
    Expected Output:
      01:30:02
      01:30:01
      01:30:00
      01:29:59
      ...
      00:00:00
*/

// const countDown = (seconds) => {
//   let timer = seconds;

//   setInterval(() => {
//     let result = new Date(timer * 1000).toISOString().slice(11, 19);
//     console.log(result);
//     timer--;
//   }, 1000)

//   let hours  = Math.floor(seconds/3600);
//   let minute = Math.floor((seconds % 3600) / 60);
//   let second = Math.floor(seconds % 60);

//   const displayCountDown = (hours, minute, second) => {
//     let time = `${hours}:${minute}:${second}`;
//   }

//   const countdownInterval = setInterval(() => {
//     seconds--;

//     hours  = Math.floor(seconds/3600);
//     minute = Math.floor((seconds % 3600) / 60);
//     second = Math.floor(seconds % 60);

//     displayCountDown(hours, minute, second);

//     if (seconds === 0){
//       clearInterval(countdownInterval);

//     }
//   }, 1000);

// }
// countDown(5402);


/**** 
2. Create program to display list recommendation anime. (you can fetching data from url: https://api.jikan.moe/v4/recommendations/anime )
    a. Show all list title
    b. Sort title by date release
    c. Show 5 most popular anime from recommendation
    d. Show 5 high rank anime from recommendation
    e. Show most episodes anime from recommendation
*****/

const response = await fetch('https://api.jikan.moe/v4/recommendations/anime');
const body = await response.json();
const data = body.data;

const response2 = await fetch('https://api.jikan.moe/v4/anime');
const body2 = await response2.json();
const data2 = body2.data;

// Limit 20 data from response
const limit = data.slice(0, 20);
const limit2 = data2.slice(0, 20);


// a.Show all list title
console.log('List of recommended anime titles:');
const title = limit.map((item) => item.entry.map((item) => 
item.title))
const merged = title.flat(); // merge array
console.log('====================================================')
console.log('Show top 5 ranks from recommendation:');
merged.forEach((title) => {
  console.log("Title : " + title);
});


// b. Sort title by date release
const sortedByRelease = limit.sort((a, b) => {
  const releaseDateA = new Date(a.date);
  const releaseDateB = new Date(b.date);
  return releaseDateB - releaseDateA;
});
console.log('====================================================')
console.log('Sort title by date release:');
sortedByRelease.forEach((item) => 
{
  console.log("Title: " + item.entry[0].title + ", Date : " + item.date);
});

// c. Show 5 most popular anime from recommendation
const sortedPopularity = limit2.sort(
  (a, b) => b.popularity - a.popularity);
const titlesByPopularity = sortedPopularity.map((item) => 
"Title : " + item.title + ", Popularity :" + item.popularity).slice(0, 5);

console.log('====================================================')
console.log('Show top 5 most popular anime from recommendation:');
titlesByPopularity.forEach((title) => {
  console.log(title);
});

// d. Show 5 high rank anime from recommendation
const rankSorted = limit2.sort(
  (a, b) => b.rank - a.rank);
const titlesByRank = rankSorted.map((item) => 
"Title : " + item.title + ", Rank :" + item.rank).slice(0, 5);

console.log('====================================================')
console.log('Show top 5 ranks from recommendation:');
titlesByRank.forEach((title) => {
  console.log(title);
});

// e. Show most episodes anime from recommendation
const sortedEpisodes = limit2.sort(
  (a, b) => b.episodes - a.episodes);
const titlesByEpisodes = sortedEpisodes.map((item) => 
"Title : " + item.title + ", Episodes :" + item.episodes);

console.log('====================================================')
console.log('Show most episodes anime from recommendation:');
titlesByEpisodes.forEach((title) => {
  console.log(title);
});

