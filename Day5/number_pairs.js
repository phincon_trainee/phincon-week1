/*
10. Create a function that determines how many number pairs are embedded 
in a space-separated string. The first numeric value in the space-separated 
string represents the count of the numbers, thus, excluded in the pairings.
Notes : 
Always take into consideration the first number in the string is not part 
of the pairing, thus, the count. It may not seem so useful as most people see it,
but it's mathematically significant if you deal with set operations.
*/
function number_pairs(str) {
    const numbers = str.split(" ").map(Number);
    
    const count = numbers.shift();
    
    const frequency = {};
    for (const num of numbers) {
      frequency[num] = (frequency[num] || 0) + 1;
    }
    
    let pairCount = 0;
    for (const num in frequency) {
      pairCount += Math.floor(frequency[num] / 2);
    }
    
    return pairCount;
  }
  
console.log(number_pairs("7 1 2 1 2 1 3 2"));
console.log(number_pairs("4 2 3 4 1") );