/* 13. A countdown sequence is a descending sequence of k integers from k down to 1 (e.g. 5, 4, 3, 2, 1). 
Write a function that returns an array of the form[x, y] where x is the number of 
countdown sequences in the given array and y is the array of those sequences in the 
order in which they appear in the array.
*/
const finalCountdown = (arr) => {
    let count = 0;
    const sequences = [];
    let currentSequence = [];
  
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === arr[i + 1] + 1) {
        currentSequence.unshift(arr[i]);
      } else {
        if (currentSequence.length > 0) {
          currentSequence.unshift(arr[i]);
          sequences.push(currentSequence);
          currentSequence = [];
          count++;
        } else {
          count++;
        }
      }
    }
  
    return [count, sequences];
  }
  console.log(finalCountdown([4, 8, 3, 2, 1, 2]));
  