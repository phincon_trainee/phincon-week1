/* 14. Create a function that takes a Tic-tac-toe board and returns "X" if the X's are 
placed in a way that there are three X's in a row or returns "O" if there are three O's in a row.
Notes: 
All places on the board will have either "X" or "O". If both "X" and "O" win, return "Tie".
*/
const whoWon = (board) => {
    // Check rows
    for (let row = 0; row < 3; row++) {
      if (board[row][0] === board[row][1] && board[row][1] === board[row][2]) {
        return board[row][0];
      }
    }
  
    // Check columns
    for (let col = 0; col < 3; col++) {
      if (board[0][col] === board[1][col] && board[1][col] === board[2][col]) {
        return board[0][col];
      }
    }
  
    // Check diagonals
    if (board[0][0] === board[1][1] && board[1][1] === board[2][2]) {
      return board[0][0];
    }
    if (board[0][2] === board[1][1] && board[1][1] === board[2][0]) {
      return board[0][2];
    }
  
    // No winner
    return "Tie";
  }

  console.log(whoWon([
    ["O", "X", "O"],
    ["X", "X", "O"],
    ["O", "X", "X"]
  ])
  );

  console.log(whoWon([
    ["O", "O", "X"],
    ["X", "O", "X"],
    ["O", "X", "O"]
  ])
  );

  console.log(whoWon([
    ["X", "O", "X"],
    ["X", "O", "O"],
    ["O", "X", "O"]
])
);
  