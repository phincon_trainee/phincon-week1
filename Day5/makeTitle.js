/* 2. Create a function that takes a string as an argument and converts 
the first character of each word to uppercase. Return the newly formatted string.
Notes :
You can expect a valid string for each test case.
Some words may contain more than one uppercase letter (see example #4)
*/

const makeTitle = (strring) => {
    let array = strring.split(' ');
    let newArray = [];

    for(let i =0; i < array.length; i++){
        newArray.push(array[i].charAt(0).toUpperCase()+array[i].slice(1));
    }

    return newArray.join(' ');
}
    console.log("2. Converts the first character of each word to uppercase : ")
    console.log(makeTitle("capitalize every word"));
    console.log(makeTitle("1f you c4n r34d 7h15, you r34lly n33d 2 g37 l41d"));