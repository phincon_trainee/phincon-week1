/*
8. Create a function that returns true if an asterisk * is inside a box.
Notes :
The asterisk may be in the array, however, it must be inside the box, if it exists.
*/

const inBox = (arr) => {
    let result = false;
    arr.slice(1, -1).forEach((item) => {
      if (/#\s*\*\s*#/.test(item)) result = true;
    });
    return result;
  };
  
  
  console.log(inBox([
    "###",
    "#*#",
    "###"
  ])
  );

  console.log(inBox([
    "####",
    "#* #",
    "#  #",
    "####"
  ]) 
  );

  console.log(inBox([
    "*####",
    "# #",
    "#  #*",
    "####"
  ])
  );

  console.log(inBox([
    "####", 
    "# *#", 
    "#  #", 
    "####"
    ])
    );
  