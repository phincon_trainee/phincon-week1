/* 4. Create a function that takes an array of items, removes all duplicate items and returns a new array in the same sequential order as the old array (minus duplicates).
Notes : 
Tests contain arrays with both strings and numbers.
Tests are case sensitive.
Each array item is unique.
*/
const removeDups = (arr) => {
    let unique = [];
    let haveItems = new Set();

    for(const item of arr){
        if(!haveItems.has(item)){
            unique.push(item);
            haveItems.add(item);
        }
    }
    return unique;
}
console.log("4. Duplicate Item")
console.log(removeDups(["John", "Taylor", "John"]) );
console.log(removeDups(['#', '#', '%', '&', '#', '$', '&']));