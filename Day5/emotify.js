/*
1. Create a function that changes specific words into emoticons. 
Given a sentence as a string, replace the words smile, grin, sad and mad 
with their corresponding emoticons
word	emoticon
smile	:D
grin	:)
sad	    :(
mad	    :P
*/

const emotify = (words) => {
    const emoticons = {
        smile : ":D",
        grin  : ":)",
        sad   : ":(",
        mad   : ":p" 
    };
     return words.replace(/smile|grin|sad|mad/gi, match => emoticons[match.toLowerCase()]);
}
    console.log("1. Changes specific words into emoticons : ")
    console.log(emotify("Make me smile"));
    console.log(emotify("Make me grin"));
    console.log(emotify("Make me sad"));
    console.log(emotify("Make me smile"));
    console.log(emotify("Make me grin"));
    console.log(emotify("Make me sad"));
    console.log(emotify("Make me mad"));

// pakai if else
const emoticon = (words) => {
    const words1 = words.split(" ")[2];
    let result = "";

    if( words1 === "smile"){
        result = ":D";
    }else if(words1 === "grin"){      
        result = ":)";
    }else if (words1 === "sad") {
        result = ":(";
    }else if (words1 === "mad") {
        result = ":P";
    }

    return "Make me" + result;
}
    console.log("\nPakai If Else :");
    console.log(emotify("Make me smile"));
    console.log(emotify("Make me grin"));
    console.log(emotify("Make me sad"));
    console.log(emotify("Make me smile"));
    console.log(emotify("Make me grin"));
    console.log(emotify("Make me sad"));
    console.log(emotify("Make me mad"));