/*
9. You are given a dictionary of names and the amount of points they have. 
Return a dictionary with the same names, but instead of points, return what prize they get.
"Gold", "Silver", or "Bronze" to the 1st, 2nd and 3rd place respectively. 
For all the other names, return "Participation" for the prize.
*/  
const awardPrizes = (arr) => {
    let trueArr = Object.entries(arr);
    let toArr = Object.entries(arr).sort((a, b) => b[1] - a[1]);
    let award = {};
  
    for (let i = 0; i < toArr.length; i++) {
      const [name] = trueArr[i];
  
      if (trueArr[i][0] === toArr[0][0]) {
        award[name] = "Gold";
      } else if (trueArr[i][0] === toArr[1][0]) {
        award[name] = "Silver";
      } else if (trueArr[i][0] === toArr[2][0]) {
        award[name] = "Bronze";
      } else {
        award[name] = "Participation";
      }
    }
    return award;
  };
  
  console.log(
    awardPrizes({
      Joshua: 45,
      Alex: 39,
      Eric: 43,
    })
  );

  console.log(awardPrizes({
    "Person A" : 1,
    "Person B" : 2,
    "Person C" : 3,
    "Person D" : 4,
    "Person E" : 102
  })
  );
  
  console.log(
    awardPrizes({
      Mario: 99,
      Luigi: 100,
      Yoshi: 299,
      Toad: 2,
    })
  );
  

