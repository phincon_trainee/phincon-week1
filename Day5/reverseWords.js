/* 7. Given an input string, reverse the string word by word, 
the first word will be the last, and so on.
*/

const reverseWords = (data) =>{
    data = data.trim(); // menghapus spasi di bagian depan dan belakang string 'data'
    const words = data.split(/\s+/); //memecah string menjadi array kata
    const reservedWords = words.reverse(); //membalikkan urutan
    const reservedString = reservedWords.join(' '); //menggabungkan elememen

    return reservedString;
}
console.log(reverseWords(" the sky is blue") );
console.log(reverseWords("hello   world!  "));
console.log(reverseWords("a good example"));
console.log(reverseWords("hello world!"));
console.log(reverseWords("blue is sky the"));
console.log(reverseWords("a good example"));
console.log(reverseWords("fraud! of example another is this"));
console.log(reverseWords("man! the are You"));
