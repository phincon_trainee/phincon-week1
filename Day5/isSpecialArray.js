/* 6. An array is special if every even index contains an even number 
and every odd index contains an odd number. Create a function that 
returns true if an array is special, and false otherwise.
*/
const isSpecialArray = (arr) => {
    for(let i = 0; i<arr.length; i++){
        if(i % 2 !== 0 && arr[i] === 0 ){ //indeks genap berisi bil ganjil
            return false;
        }else if(i % 2 === 0 && arr[i] !== 0){ //indeks ganjil berisi bil genap
            return false;
        }
    }
    return true;
}
console.log(isSpecialArray(isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3])));
console.log(isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]));
console.log(isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]));
console.log(isSpecialArray([1, 1, 1, 2]));
console.log(isSpecialArray([4, 5, 6, 7, 0, 5]));
