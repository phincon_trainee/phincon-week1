/* 11. Abigail and Benson are playing Rock, Paper, Scissors.
Each game is represented by an array of length 2, 
where the first element represents what Abigail played 
and the second element represents what Benson played.
*/
const calculateScore = (games) =>{
    let abigailScore = 0;
    let BensonScore = 0;

    for(const game of games){
        const abigailPlay = game[0];
        const bensonPlay = game[1];

        if (
            (abigailPlay === "R" && bensonPlay === "S") ||
            (abigailPlay === "S" && bensonPlay === "P") ||
            (abigailPlay === "P" && bensonPlay === "R")
        ){
            // Abigail wins
            abigailScore++;
        }else if(
            (bensonPlay === "R" && abigailPlay === "S") ||
            (bensonPlay === "S" && abigailPlay === "P") ||
            (bensonPlay === "P" && abigailPlay === "R")
        ){
            // Benson wins
            BensonScore++;
        }
    }
    if (abigailScore > BensonScore) {
        return "Abigail";
      } else if (abigailScore < BensonScore) {
        return "Benson";
      } else {
        return "Tie";
      }
}
console.log(calculateScore([["R", "R"], ["S", "S"]]));
console.log(calculateScore([["S", "R"], ["R", "S"], ["R", "R"]]));
console.log(calculateScore([['R', 'P'], ['R', 'S'], ['S', 'P']]));
console.log(calculateScore([['R', 'R'], ['S', 'S']]));
console.log(calculateScore([['S', 'R'], ['S', 'R'], ['S', 'R'], ['R', 'S'], ['R', 'S']]));

