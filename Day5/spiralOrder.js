/*
12. Given a matrix of m * n elements (m rows, n columns) ➞ return all elements of the matrix in spiral order.
*/
const spiralOrder = (matrix) => {
    let kanan = matrix[0];
    let bawah = matrix[1].slice(-1);
    let kiri = matrix[2].reverse();
    let atas = matrix[1].slice(0, -1);
  
    let result = [kanan, bawah, kiri, atas].flat();  

    return result;
}
console.log(spiralOrder([
    [ 1, 2, 3 ],
    [ 4, 5, 6 ],
    [ 7, 8, 9 ]
  ])
  );
